CREATE DATABASE gestion_poker;
USE gestion_poker;

CREATE TABLE joueurs(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	pseudo VARCHAR(100),
	bankroll_total DECIMAL(9,2)
	)

CREATE TABLE rooms(
	nom VARCHAR(100) PRIMARY KEY,
	budget DECIMAL(9,2)
	)

CREATE TABLE tournois(
	id VARCHAR(200) PRIMARY KEY,
	nom TEXT,
	DATE DATE, 
	type VARCHAR(200),
	buyin DECIMAL(9,2),
	nombre_entrants MEDIUMINT UNSIGNED, 
	place MEDIUMINT UNSIGNED, 
	gain DECIMAL(9,2),
	room VARCHAR(100),

	CONSTRAINT fk_tournoi_room
			FOREIGN KEY (room)
			REFERENCES rooms(nom)
	)

CREATE TABLE joueur_room(
	id_joueur INT UNSIGNED,
	id_room VARCHAR(100),

	CONSTRAINT fk_joueurroom_joueur
			FOREIGN KEY (id_joueur)
			REFERENCES joueurs(id),

	CONSTRAINT fk_joueurroom_room
			FOREIGN KEY (id_room)
			REFERENCES rooms(nom),

	CONSTRAINT PRIMARY KEY joueur_room (id_joueur, id_room)
	)

CREATE TABLE tournoi_joueur(
	id_tournoi VARCHAR(200),
	id_joueur INT UNSIGNED,

	CONSTRAINT fk_tournoijoueur_tournoi
			FOREIGN KEY (id_tournoi)
			REFERENCES tournois(id),

	CONSTRAINT fk_tournoijoueur_joueur
			FOREIGN KEY (id_joueur)
			REFERENCES joueurs(id),

	CONSTRAINT PRIMARY KEY tournoi_joueur (id_tournoi, id_joueur)
	)
;

DESCRIBE tournois
DESCRIBE joueurs;